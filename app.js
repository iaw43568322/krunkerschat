require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var mongoose = require('mongoose');
var path = require("path")
var app = express();
var router = express.Router();
var body_parser = require('body-parser');
app.use(body_parser.urlencoded({extended: false}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
var mongoDB = 'mongodb://127.0.0.1:27017/mydb';

/* //BD
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:')); */

//Connect to mongodb://devroot:devroot@mongo:27017/chat?authSource=admin
mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
    if (err) console.log(`ERROR: connecting to Database. ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);
    

// Importamos el controlador incidence
//var ctrlIncidence = require("./app/controllers/incidence");

app.set("views", path.join(__dirname, "/views"));
app.set("view engine", "jade");



var server =  require("http")
    .createServer(app)
    .listen(port, () => {
        console.log("Servidor escuchando por el puerto: " + port);
    });

// Creamos un servicio de io y que va usar la estructura del server
var io = require("socket.io")(server);

app.set(express.static(__dirname + "/views"));

// Bootstrap
app.use("/css", express.static(__dirname + "/node_modules/bootstrap/dist/css"));
app.use("/js", express.static(__dirname + "/node_modules/jquery/dist"));
//app.use("/js", express.static(__dirname + "/node_modules/popper.js/dist"));
app.use("/js", express.static(__dirname + "/node_modules/bootstrap/dist/js"));

// Directorio js
app.use('/public/js', express.static(__dirname +  "/public/js"));


// Router
var indexRouter = require("./app/routes/base");
// Importamos rutas de la api
var apiRouter = require("./app/api/index");
// Incidence routes
var incidenceRoutes = require(__dirname + "/app/routes/incidenceRoutes");
// Chat routes
var chatRoutes = require(__dirname + "/app/routes/chatRoutes");

app.use('/', indexRouter);
// Usamos rutas api
app.use('/api', apiRouter);
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(404).send('Not Found!');
});

// Cada vez que usemos /incidence, usamos lo que hay en incidenceRoutes
app.use('/listarIncidencias', incidenceRoutes);

// Cada vez que usemos /chat, usamos lo que hay en chatRoutes
app.use('/chat', chatRoutes);
//app.use('/prueba', indexRouter);

//router.route("/incidencias").get(ctrlIncidence.listIncidences);
//app.use("/api", router);

// Controladores
var chatCtrl = require("./app/controllers/chat");

// Cuando alguien se conecte, escucha a traves de un socket determinad
// Es un vinculo entre el cliente que se acaba de conectar y el cliente
io.on("connection", (socket) => {
    console.log("Usuario conectado");
    // socket.user="Pedro";
    // Canal por defecto
    var canal = "all";
    socket.join(canal);
    // Cuando recibe algo por el canal newMsg, recibe la información y haz algo
    socket.on("newMsg", (data) => {
        console.log(data);
        // Se lo comunicamos a todos aquellos que esten en el mismo canal
        // socket.broadcast.emit("newMsg", data);
        chatCtrl.save(data);
        socket.broadcast.to(canal).emit("newMsg", data);
    })
    // Al cambiar de canal
    socket.on("changeChannel", function(newCanal) {
        socket.leave(canal);
        socket.join(newCanal);
        canal = newCanal;
        // var canalCarga = {chanel: newCanal}
        console.log("canal:" + newCanal);
        //chatCtrl.getChat(canal);
        socket.emit("changeChannel", newCanal);
    })
})

module.exports = app;