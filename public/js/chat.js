$(document).ready(function() {
    const socket = io();

    $('#selectRoom').on("change",()=>{
        var sala = $(this).find("option:selected").val();
        sessionStorage.setItem("canal", sala);
        socket.emit("changeChannel", $('#selectRoom').val());
        window.location.href = "/chat/"+sala;
        // window.history.pushState(null,"Chat","/chat/"+sala);
      })

    $("#enviar").click(function() {
        var msg = $("#inputMessage").val();
        var autor = $("#inputAutor").val();
        var sala = $("#selectRoom").find(":selected").val();
        $("#chat").append(autor + ": " + msg + "<br>");
        $("#inputMessage").val('');
        var toSend = {user:autor, msg: msg, chanel: sala};
        socket.emit("newMsg", toSend);
    }) 
    // Cuando recibimos algo por el canal, lo mostramos en el chat
    socket.on("newMsg", (data) => {
        console.log(data);
        $("#chat").append(data.user + ": " + data.msg + "<br>");
    })

    //Acciones a realizar cuando se detecta actividad en el canal changeChannel
    socket.on("changeChannel", function(channel) {
        console.log("cambiando cnaal" + channel);
        $("#chat").html('').append(`<p>Bienvenido al canal ${channel}<p>`)
    })


});