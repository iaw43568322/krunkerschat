var mongoose = require("mongoose");
Schema = mongoose.Schema;

var chatSchema = new Schema ({
    user: {type: String},
    msg: {type:String},
    chanel: {type:String},
    create_at: {type: Date,default: Date.new},
});

module.exports = mongoose.model("Chat", chatSchema);