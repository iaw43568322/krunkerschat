var mongoose = require("mongoose");
Schema = mongoose.Schema;

// Modelo incidence
var incidenceSchema = new Schema ({
    name: {type: String},
    dni: {type: String},
    type: {type: String},
    urgency: {type: String},
    description: {type: String},
    status: {type: String,default: "abierta"},
    create_at: {type: Date,default: Date.new},
});

module.exports = mongoose.model("Incidence", incidenceSchema);