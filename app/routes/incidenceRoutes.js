var express = require("express");
var router = express.Router();

router.route('/').get((req, res, next) => {
    res.render("listarIncidencias");
});
module.exports = router;