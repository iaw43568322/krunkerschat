var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();

// Import api

// var api = require("/app/app/api/request/request");


// Incidence Controller
var incidenceCtrl = require(path.join(ctrlDir, "incidence"));
// Chat controller
var chatCtrl = require(path.join(ctrlDir, "chat"));
// User Controller
var userCtrl = require(path.join(ctrlDir, "user"));

router.get("/", function(req, res, next) {
    res.render("index", {title: "Express"});
});

router.post("/addIncidence", (req, res) => {
    console.log("prueba")
    console.log(req.body);
    incidenceCtrl.addIncidence(req, res);
    res.redirect("/listarIncidencias");
    /* console.log("Req: ", req.body);
    res.json({msg: ""}); */
});

router.route("/deleteIncidence").post( async(req, res) => {
    const result = incidenceCtrl.deleteIncidence(req, res);
    if (result) {
        console.log("Incidencia eliminada");
    }

    res.redirect("/listarIncidencias");
    
});

router.route("/formularioUpdate").post(async(req, res, next) => {
    const result = await incidenceCtrl.getIncidenceById(req, res, next);
    res.render("updateIncidence", {incidence: result});
    console.log(result);
});

router.route("/updateIncidence").post(async(req, res, next) => {
    const result = await incidenceCtrl.updateIncidence(req, res, next);
    if(result) {
        console.log("Incidencia actualitzada");
    }
    res.redirect("/listarIncidencias");
    
});

// Ruta para mostrar la vista listarIncidencias.jade
// router.get("/listarIncidencias", function(req, res, next) {
//     // api.call("/incidences")
//     // .then(function(val) {
//     //     res.render("listarIncidencias", {incidences:val});
//     // })
//     var result = incidenceCtrl.listIncidences();
//     res.render("listarIncidencias", result);
// });
router.route("/listarIncidencias").get(async(req, res, next) => {
    const result = await incidenceCtrl.listIncidences(req, res, next);
    res.render("listarIncidencias", {incidences: result});
});


// router.get("/chat", (req, res, next) => {
//     api.call("/chat/load")
//     .then(function(val) {
//         res.render("chat", {msg:val});
//         console.log(val);
//     })
//     .catch(function(err) {
//         console.log(err);
//     });

//     console.log("se esta consultando");
// });

router.route("/chat").get(async(req, res, next) => {
    const result = await chatCtrl.getChat(req, res, next);
    console.log(result);
    res.render("chat", {messages: result, canal: "all"});
});

router.route("/chat/:id").get(async(req, res, next) => {
    var canal = req.url.split("/")[2];
    const result = await chatCtrl.getChatByChanel(canal);
    res.render("chat", {messages: result, canal: canal});
});

router.get("/formulario", function(req, res, next) {
    res.render("formulario", {title: "Express"});
});

module.exports = router;