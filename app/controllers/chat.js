var mongoose = require("mongoose"),
    Chat = require("../models/Chat");
// Get chat
exports.getChat = async(req, res, next) => {
    try {
        var result = await Chat.find({});
        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('Could not fecth chat ${error}');
    }
}

exports.getChatByChanel = async(req, res, next) => {
    try {
        if (req != "all") {
            result = await Chat.find({"chanel": req});
        } else {
            result = await Chat.find({});
        }
        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('Could not fecth chat ${error}');
    }
}

exports.save = (req) => {
    var newChat = new Chat(req);
    newChat.save((err, res) => {
        if(err) console.log(err);
        console.log("Insertado en la DB");
        return res;
    });
}