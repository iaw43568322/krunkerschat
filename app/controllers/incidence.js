var mongoose = require("mongoose"),
    Incidence = require("../models/Incidence");

exports.listIncidences = async(req, res, next) => {
    try {
        var result = await Incidence.find({})
        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('Could not fecth chat ${error}');
    }
    // return res.json(incidences);
};

// Add incidence
exports.addIncidence = (req, res, next) => {
    
    var newIncidence = new Incidence(req.body);
    newIncidence.save((err, res) => {
        if(err) console.log(err);
        console.log("Insertado en la DB");
        return res;
    });
    
    
    /* return res.status(200).send( {
        id: 3,
        nombre: "Jorge",
        dni: "50596873Y",
        tipo: "Filosofia",
        urgencia:"media",
        descripción: "Ser o no ser"
    }); */
    // incidences.put(req);
    // return res.json(incidences);
};

// Update incidence
exports.updateIncidence = async (req, res, next) => {

    // console.log(req.body.name);
    
    const incidence = {
        name: req.body.name,
        dni: req.body.dni,
        type: req.body.type,
        urgency: req.body.urgency,
        description: req.body.description,
        status: req.body.status
    }

    try {
        const incidenceAct = await Incidence.updateOne({_id: req.body.idIncidence}, incidence);
        return incidenceAct;
    } catch (error) {
        console.log("ERROR: " + error);
    }

    /* return res.status(200).send(  {
        id: 2,
        nombre: "Pau",
        dni: "87654321T",
        tipo: "anatomia",
        urgencia: "alta",
        descripción: "esto es una incidencia"
     }
   ); */
};

// Get incidence by id
exports.getIncidenceById = async(req, res, next) => {

    // console.log(req.body.idIncidence);

    const incidence = await Incidence.findById(req.body.idIncidence);

    /* var incidence =
    {
        id: 1,
        nombre: "Marc",
        dni: "12345678F",
        tipo: "hardware",
        urgencia: "baja",
        descripción: "esto es una incidencia"
    }; */
    // return res.json(incidence);
    return incidence;
}

// Get incidence by status
exports.getIncidenceByStatus = (req, res, next) => {
    var incidences = 
    [
      {
         id: 3,
         nombre: "Jorge",
         dni: "50596873Y",
         tipo: "Hardware",
         urgencia:"media",
         estado: "abierta",
         descripción: "Ser o no ser"
      },
      {
         id: 5,
         nombre: "Antonio",
         dni: "86954578M",
         tipo: "Software",
         urgencia:"media",
         estado: "abierta",
         descripción: "Solo se que no se nada"
      }
    ];
    
    return res.json(incidences);
}

// Delete incidence
exports.deleteIncidence = async (req, res, next) => {

    try {
        const incidence = await Incidence.findById(req.body.idIncidence);
        const result = incidence.delete();
        

        return result;        
    } catch (error) {
        console.log("ERROR: " + error);
    }
    
    
    
}