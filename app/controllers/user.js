// List users
exports.listUsers = (req, res, next) => {
    var users = 
    [
        {
            id: 1,
            nombre: "Ramon",
            apellidos: "Garcia Gijon",
            dni: "50596873Y"
        },
        {
            id: 2,
            nombre: "Sara",
            apellidos: "Rivera Cantos",
            dni: "496587451S"
        }
       
    ];
    if (req.isApi) {
        res.status(200).jsonp(users);
    } else {
        return users;
    }
};