var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();

// Incidence Controller
var incidenceCtrl = require(path.join(ctrlDir, "incidence"));
// User Controller
var userCtrl = require(path.join(ctrlDir, "user"));
// Chat Controller
var chatCtrl = require(path.join(ctrlDir, "chat"));

// We use this middlewere to define the request as API type
router.use(function timeLog(req, res, next) {
    req.isApi = true;
    next();
});


/** INCIDENCE ROUTES */
// List incidences
router.get("/incidences", incidenceCtrl.listIncidences);
// Add incidence
router.post("/addIncidence", incidenceCtrl.addIncidence);
// Update incidence
router.put("/incidences/:id", incidenceCtrl.updateIncidence);
// Get incidence by id
router.get("/incidences/:id", incidenceCtrl.getIncidenceById);
// Get incidences by status (CHECK)
router.get("/incidences/status/:status", incidenceCtrl.getIncidenceByStatus);
// Update incidence's status
//router.put("/incidences/:status", incidenceCtrl.updateIncidence);
// Delete incidence
router.delete("/incidences/:id", incidenceCtrl.deleteIncidence);

/** USER ROUTES */
router.get("/users", userCtrl.listUsers);

/** CHAT ROUTES */
router.get("/chat/load", chatCtrl.getChat);
module.exports = router;